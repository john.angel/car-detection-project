import os
import psycopg2

def initialize_database(dbname, dbuser, dbpassword, dbhost):
  conn = psycopg2.connect(dbname=dbname, user=dbuser, password=dbpassword, host=dbhost)

  cur = conn.cursor()

  sql = """CREATE TABLE IF NOT EXISTS car_record (
                                id SERIAL PRIMARY KEY,
                                image_name VARCHAR(255) NOT NULL,
                                number INTEGER NOT NULL
  );"""

  try:
    cur.execute(sql)
    conn.commit()
    print(f"Table car_record table created successfully in database '{dbname}'.")
  except Exception as error:
    print(f"Error creating table: {error}")
  finally:
    cur.close()
    conn.close()

dbname = os.getenv("DB_NAME")
dbuser = os.getenv("DB_USER")
dbpassword = os.getenv("DB_PASS")
dbhost = os.getenv("DB_HOST")

initialize_database(dbname, dbuser, dbpassword, dbhost)
