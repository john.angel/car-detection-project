import psycopg2
import os

dbname = os.getenv("DB_NAME")
dbuser = os.getenv("DB_USER")
dbpassword = os.getenv("DB_PASS")
dbhost = os.getenv("DB_HOST")


# Replace with your connection details
connection = psycopg2.connect(
    database=dbname,
    user=dbuser,
    password=dbpassword,
    host=dbhost
)

connection.set_session(autocommit=True)

cursor = connection.cursor()


insert_query = "INSERT INTO car_record (image_name, number) VALUES (%s, %s)"

def save_record(name, num_of_cars):
    data = (name, num_of_cars)
    cursor.execute(insert_query, data)
    connection.commit()