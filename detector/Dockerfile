FROM debian:11-slim AS build
RUN DEBIAN_FRONTEND=noninteractive apt-get update && \
    apt-get install --no-install-suggests --no-install-recommends --yes python3-venv gcc libpython3-dev libgl1-mesa-glx libglib2.0-0 libsm6 libxrender1 libxext6 libpcre3 python3-opencv python3-psycopg2 libpq-dev && \
    python3 -m venv /venv && \
    /venv/bin/pip install --upgrade pip setuptools wheel

FROM build AS build-venv
COPY requirements.txt /requirements.txt
RUN /venv/bin/pip install --disable-pip-version-check -r /requirements.txt


FROM gcr.io/distroless/python3-debian11
ENV PYTHONUNBUFFERED=0
COPY --from=build-venv /venv /venv
COPY . /app
WORKDIR /app
COPY --from=build /usr/lib/ /usr/lib/
COPY --from=build /lib /lib
ENTRYPOINT ["/venv/bin/python3", "app.py"]