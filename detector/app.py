#!/bin/python3
import pika
import os
import json
import cv2
import numpy as np
import logging
import sys
import base64

import db

logger = logging.getLogger(__name__)
logging.basicConfig(filename='myapp.log', level=logging.INFO)
handler = logging.StreamHandler(sys.stdout)
formatter = logging.Formatter("%(asctime)s - %(levelname)s - %(message)s")
handler.setFormatter(formatter)
logger.addHandler(handler)
# Load YOLO
net = cv2.dnn.readNet("yolov3.weights", "yolov3.cfg")
# Get output layer names
output_layers = net.getUnconnectedOutLayersNames()

def process_image(channel, method, properties, body):
# logger.info(f"Received message: {body.decode('utf-8')}")

# Extract image data from the JSON message
    try:
        image_data = json.loads(body)
        filename = image_data['filename']
        image_bytes = image_data['data']
        # logger.info(image_bytes)
    except json.JSONDecodeError:
        logger.info("Error: Invalid JSON message format")
        return

    image_bytes = base64.b64decode(image_bytes)

    img = cv2.imdecode(np.frombuffer(image_bytes, np.uint8), cv2.IMREAD_COLOR)

    if img is None:
        return

    img = cv2.resize(img, None, fx=0.4, fy=0.4)
    height, width, channels = img.shape

    # Detecting objects
    blob = cv2.dnn.blobFromImage(img, 0.00492, (416, 416), (0, 0, 0), True, crop=False)
    net.setInput(blob)
    outs = net.forward(output_layers)

    # Showing information on the screen
    class_ids = []
    confidences = []
    boxes = []
    for out in outs:
        for detection in out:
            scores = detection[5:]
            class_id = np.argmax(scores)
            confidence = scores[class_id]
            if confidence > 0.5:  # Lowered confidence threshold
                # Object detected
                center_x = int(detection[0] * width)
                center_y = int(detection[1] * height)
                w = int(detection[2] * width)
                h = int(detection[3] * height)

                # Rectangle coordinates
                x = int(center_x - w / 2)
                y = int(center_y - h / 2)

                boxes.append([x, y, w, h])
                confidences.append(float(confidence))
                class_ids.append(class_id)

        # Apply non-maximum suppression
    indexes = cv2.dnn.NMSBoxes(boxes, confidences, 0.2, 0.1)  # Adjusted NMS parameters

    num = len(indexes)
    logger.info(f"Found: {num} cars in {filename}")
    db.save_record(filename, num)

if __name__ == "__main__":
    
    logger.info('Starting...')
    HOST = os.getenv('MESSAGE_QUEUE')
    connection = pika.BlockingConnection(pika.ConnectionParameters(host=HOST))
    channel  = connection.channel()

    queue_name = os.getenv('QUEUE_NAME')
    channel.queue_declare(queue=queue_name)

    channel.basic_consume(queue=queue_name, on_message_callback=process_image)
    logger.info("Waiting for messages")
    channel.start_consuming()

    logger.info('Shuting down...')