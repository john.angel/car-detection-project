module car-detection-frontend

go 1.21.9

require (
	github.com/PuerkitoBio/goquery v1.9.2 // indirect
	github.com/andybalholm/cascadia v1.3.2 // indirect
	github.com/disintegration/imaging v1.6.2 // indirect
	github.com/jmoiron/sqlx v1.4.0 // indirect
	github.com/lib/pq v1.10.9 // indirect
	github.com/streadway/amqp v1.1.0 // indirect
	golang.org/x/image v0.16.0 // indirect
	golang.org/x/net v0.24.0 // indirect
)
