package main

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"

	"html/template"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"github.com/streadway/amqp"
)

func uploadHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Println("File Upload Endpoint Hit")

	// Parse our multipart form, 10 << 20 specifies a maximum
	// upload of 10 MB files.
	r.ParseMultipartForm(int64(imageMaxSize))
	// FormFile returns the first file for the given key `myFile`
	// it also returns the FileHeader so we can get the Filename,
	// the Header and the size of the file
	file, handler, err := r.FormFile("myFile")
	if err != nil {
		fmt.Println("Error Retrieving the File")
		fmt.Println(err)
		return
	}
	defer file.Close()
	fmt.Printf("Uploaded File: %+v\n", handler.Filename)
	fmt.Printf("File Size: %+v\n", handler.Size)
	fmt.Printf("MIME Header: %+v\n", handler.Header)

	imageBytes, err := io.ReadAll(file)

	if err != nil {
		log.Printf("Error reading image data: %v\n", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	imageData := map[string]interface{}{
		"filename": handler.Filename,
		"data":     imageBytes,
	}

	err = sendToRabbitMQ(imageData)
	if err != nil {
		log.Printf("Error sending image to RabbitMQ: %v\n", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "text/html")
	html := `<h1>Image Sent!</h1>`

	w.Write([]byte(html))
}

func sendToRabbitMQ(data map[string]interface{}) error {
	conn, err := amqp.Dial(rabbitMQURL)
	if err != nil {
		return fmt.Errorf("failed to connect to RabbitMQ: %v", err)
	}
	defer conn.Close()

	ch, err := conn.Channel()
	if err != nil {
		return fmt.Errorf("failed to open a channel: %v", err)
	}
	defer ch.Close() // Close the channel on exit

	_, err = ch.QueueDeclare(
		queueName, // Or your exchange name
		false,     // Don't fail if already exists
		false,     // Don't mark messages persistent
		false,     // Don't delete when no consumers
		false,
		nil, // No additional arguments
	)

	if err != nil {
		return fmt.Errorf("failed to declare queue/exchange: %v", err)
	}

	body, err := json.Marshal(data)

	if err != nil {
		return fmt.Errorf("failed to marshal image data: %v", err)
	}

	err = ch.Publish(
		"",        // Empty string for default exchange
		queueName, // Or your exchange name
		false,     // Don't set mandatory flag
		false,     // Don't set immediate flag
		amqp.Publishing{
			ContentType: "application/json", // Set content type
			Body:        body,
		},
	)

	if err != nil {
		return fmt.Errorf("failed to publish message: %v", err)
	}

	log.Printf("Succesfully sent message")
	return nil
}

func adminHandler(w http.ResponseWriter, r *http.Request) {
	log.Println("admin endpoint hit")
	connectStr := fmt.Sprintf("user=%s dbname=%s sslmode=disable password=%s host=%s", dbUser, dbName, dbPassword, dbHost)
	db, err := sqlx.Connect("postgres", connectStr)

	if err != nil {
		log.Fatalln(err)
	}

	defer db.Close()
	rows, err := db.Query("Select image_name, number from car_record;")

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	var data []Record
	for rows.Next() {
		var record Record
		err := rows.Scan(&record.ImageName, &record.NumOfCars)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		data = append(data, record)
	}

	err = adminTmpl.Execute(w, data)
	if err != nil {
		log.Println(err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	log.Println("Succesfully rendedered")
}

type Record struct {
	ImageName string
	NumOfCars int
}

var (
	rabbitMQURL  = ""                                                              // Replace with your RabbitMQ connection details
	queueName    = ""                                                              // Adjust the queue name as needed
	adminTmpl    = template.Must(template.ParseFiles("templates/admin_page.html")) // 10 MB maximum image size
	imageMaxSize = 10 * 1024 * 1024
	dbUser       = ""
	dbName       = ""
	dbPassword   = ""
	dbHost       = ""
)

func main() {
	rabbitMQURL = os.Getenv("MESSAGE_QUEUE")
	queueName = os.Getenv("QUEUE_NAME")
	dbUser = os.Getenv("DB_USER")
	dbName = os.Getenv("DB_NAME")
	dbPassword = os.Getenv("DB_PASS")
	dbHost = os.Getenv("DB_HOST")

	log.Printf("Accessing rabbitmq at %s:%s", rabbitMQURL, queueName)

	fs := http.FileServer(http.Dir("./templates"))
	http.Handle("/", fs)
	http.HandleFunc("/upload", uploadHandler)
	http.HandleFunc("/admin", adminHandler)
	log.Printf("Server started on port 3000")
	log.Fatal(http.ListenAndServe(":3000", nil))
}
