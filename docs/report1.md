# CI/CD környezet kialakítása

Nagyon sok CI/CD tooling közül választhatunk, az én választásom mégis a `Gitlab`-ra esett. Korábbi projektekhez használtam már `Jenskins`-t, de nem voltam megelégedve vele. A pipelineokat `Groovy` nyelven kell benne leírni ami régebbi `Java` szintaxisra hasonlíthat, nagyon sok hibát okozott. Valamint az sem tetszett hogy a nem jelenik meg a legtöbb verzió kezelő webes felületén. Illetve az sem tetszik hogy nincs a forráskód mellett a pipeline kódja ez esetleges feature branchek esetén jelentett plussz ráfordítást.

A Gitlab CI ezzel ellen `Bash`-ben kell leírni egy `.gitlab-ci.yml` fájlban amit a repository gyökerébe helyezünk. A repository-nk tetején láthatjuk legutóbbi futás eredményét: siker esetén pipa zöld körben, hiba esetén fehér x piros körben. Ha erre az indikátorra kattintunk láthatjunk a pipeline-ban leírt rész folyamatok gráfját.
Egy folyamatra való kattintáskor láthatjuk annak logjait és a parancsok kimenetét.

## CICD telepítése

A GitLab Runner a pipeline-okban meghatározott feladatok futtatásáért felelős végrehajtó 'agent'. A GitLab alapértelmezés szerint nem tartalmaz futót. Számos  módszerek áll rendelkezésünkre a `runner` telepítésére: választhatjuk, simán binárisból telepítjük vagy letölthetjünk ha disztribuciónknak van előre lefordított csomagja, akár Docker konténerben futtatjuk, vagy könnyedén telepíthetjük a Helm segítségével Kubernetesben.

Én a Helm telepítést választottam a K8s klaszter megléte és az egyszerűsége miatt. A telepítési folyamat elindítása előtt megtettem a szükséges lépéseket, hogy egy YAML fájlban előkészítsem a szükséges értékeket, például a szerver URL-jét és a használandó 'ServiceAccount' nevét. Emellett a regisztrációs tokent Kubernetes Secretben is létrehoztam.

Az általam kialakított pipeline a következő:  

```yaml
stages:
  - build
  - doc🖹

hf1:
  image:
    name: texlive/texlive
    entrypoint: [ "" ]
  stage: doc🖹
  before_script:
    - export DEBIAN_FRONTEND=noninteractive && apt-get update
    - apt-get -y install pandoc librsvg2-bin
  script:
    - pandoc --pdf-engine=lualatex --metadata-file=metadata.yaml hf1/report.md -o s50lxk.pdf -f markdown+implicit_figures
  artifacts:
    paths:
      - s50lxk.pdf
  rules:
    - changes:
        - hf1/report.md

lint:
  image:
    name: python:slim
    entrypoint: [ "" ]
  stage: build
  before_script:
    - export DEBIAN_FRONTEND=noninteractive && apt-get update
    - pip install ruff
  script:
    - python -m ruff check .

variables:
  CONTEXT: "detector/"
  DOCKERFILE: detector/
  REF: "latest"

build-container:
  image: 
    name: moby/buildkit
    entrypoint: [ "" ]
  stage: build
  before_script:
    - BASE64_AUTH=`echo -n "$CI_REGISTRY_USER:$CI_REGISTRY_PASSWORD" | base64`
    - mkdir -p ~/.docker
    - >
      echo "{\"auths\": {\"$CI_REGISTRY\": {\"auth\": \"$BASE64_AUTH\"}}}" > ~/.docker/config.json
    - if [ "$REF" = "" ]; then REF="$CIREF"; fi
    - echo $CI_REGISTRY_IMAGE:$REF
  script:
    - >
      buildctl-daemonless.sh build --progress=plain
      --frontend=dockerfile.v0 --local context=$CONTEXT --local dockerfile=$DOCKERFILE
      --output type=image,name=$CI_REGISTRY_IMAGE:$REF,push=true,compression=estargz

```

## Telepítés

Az alkalmazás stack telepítésére is Gitlabot fogok használni. Lehetőség van a repositorynkban lévő `Helm` chartok autimatikus frissítésére pusztán Gitlab használatával. Ehhez csupán össze kell kapcsolni a repositorynkat a Kubernetes klaszterünkkel. A Gitlab agent telepítési folyamatát nem fejtem ki részletesen.

## Ent-2-end Tesztelés

Az End-to-end tesztekre a Helm test hookjait fogom használni. Ezek segítsével olyan Kubernetesben futó teszteket írhatunk amiket összecsomagoltunk az alkalmazásunkkal de teszt végeztével automatikusan törlődnek. A teszteket leíró fájlokat is a chart `templates` könyvtárába teszük, ezek a chartokhoz hasonlóan paraméterezhetőek. 
