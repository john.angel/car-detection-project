# Házi feladat

## Kigondolt architektúra

![Arhitektúra](docs/figs/arch.png)  


## Frontend

Felelősségei:
- képek kirendelelése
- Feltöltött kép újra méretezése ha túl nagy

Frontendnek egy egyszerű HTML weblapom van amit egy GO-ban implementált fájl szerver szolgál ki. Ha a kép felbontása túl nagy(ebben az esetben 1080x720 pixelnél nagyobb) akkor újra méterezi az arányok megtartásával. A HTML fájlban található Javascript végzi a képújra méretezését és teszi lehetővé hogy az egérrel is behúzhassunk képeket.

## Auto felismeres

Az autó felismeréshez a YOLOv3 előratanított modellt használtam Python SDK-val. A python program be azonosítja és bekeretezi a talált autókat, majd vissza küldi a képet az autók számával együtt. Amikor egy feldolgozó végzett elveszik a következő feldolgozandó képet.

