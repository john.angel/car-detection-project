# Üzemeltetők feliratkozási funkciója

A feliratkozási funkciót egyszerűen és hatékonyan akartam megvalósítani, összhangban az oldal általános kialakításával. Eleinte emailes vagy chatbotos megoldáson gondolkodtam, de a leírás alaposabb áttanulmányozása után a Message Queue használata tűnt a legmegfelelőbbnek.

Ennek alapján kiválasztottam a feltételezhetően legnépszerűbb Message Queue megoldást, a RabbitMQ-t. Alapvetően AMQP és STOMP protokollokat használ, de ezeknek a weboldal integrációja elsőre bonyolultnak tűnt. Szerencsére a STOMP protokollhoz találtam egy plugint, ami WebSocketen keresztül teszi lehetővé az üzenetek továbbítását.


A frontend módosult kódja:  
```
const (
	RABBITMQ_HOST     = "rabbitmq"
	RABBITMQ_PORT     = 15674
	QUEUE_NAME        = "car_numbers"
	ADMIN_ROUTE       = "/admin"
	UPLOAD_ROUTE      = "/upload"
	UPLOAD_METHOD     = http.MethodPost
	ORIGINAL_IMAGE_KEY = "original_image_path"
	MODIFIED_IMAGE_KEY = "modified_image_path"
)

type Upload struct {
	NumCars            int    `json:"num_cars"`
	OriginalImagePath  string `json:"original_image_path"`
	ModifiedImagePath string `json:"modified_image_path"`
	Text               string `json:"text"`
}

func detectCars(imagePath string) (int, string) {
	// Replace this with your actual car detection logic
	// This is a placeholder to demonstrate the concept
	return 2, "Image processed with annotations"
}

func sendToRabbitMQ(numCars int, text string) error {
	conn, err := amqp.Dial(fmt.Sprintf("amqp://%s:%d", RABBITMQ_HOST, RABBITMQ_PORT))
	if err != nil {
		return fmt.Errorf("failed to connect to RabbitMQ: %w", err)
	}
	defer conn.Close()

	ch, err := conn.Channel()
	if err != nil {
		return fmt.Errorf("failed to open a channel: %w", err)
	}
	defer ch.Close()

	_, err = ch.QueueDeclare(
		QUEUE_NAME,
		false,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		return fmt.Errorf("failed to declare queue: %w", err)
	}

	body, err := json.Marshal(map[string]interface{}{
		"num_cars": numCars,
		"text":     text,
	})
	if err != nil {
		return fmt.Errorf("failed to marshal message: %w", err)
	}

	err = ch.Publish(
		"",    // exchange (empty string for default)
		QUEUE_NAME,
		false,
		false,
		amqp.Publishing{
			DeliveryMode: amqp.DeliveryTransient,
			ContentType:  "application/json",
			Body:        body,
		},
	)
	if err != nil {
		return fmt.Errorf("failed to publish message: %w", err)
	}

	fmt.Println("Sent message to RabbitMQ")
	return nil
}

func adminHandler(w http.ResponseWriter, r *http.Request) {
	// Serve your admin template here (e.g., using html/template)
	fmt.Fprintln(w, "Admin Page")
}
```